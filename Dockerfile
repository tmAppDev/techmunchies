# Build
FROM node:10-slim as build

WORKDIR /build

RUN apt-get update \
    && apt-get install -y build-essential libpng-dev zlib1g-dev

COPY ./ ./

RUN npm i
RUN npm i -g gatsby-cli
RUN gatsby build

COPY ./config/nginx.conf /build/public/nginx.conf

# Prod
FROM nginx:mainline-alpine

WORKDIR /public

COPY --from=build /build/public ./
RUN rm /etc/nginx/nginx.conf \
    && mv /public/nginx.conf /etc/nginx/nginx.conf \
    && mkdir -p /run/nginx

EXPOSE 80

HEALTHCHECK --interval=30s --timeout=5s --start-period=10s \
        CMD wget --quiet --tries=1 --spider http://localhost/ || exit 1

STOPSIGNAL SIGTERM

CMD [ "nginx" ]
