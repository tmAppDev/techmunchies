import Typo from 'typography'
import githubTheme from 'typography-theme-github'

const Typography = new Typo(githubTheme)

export default Typography
