import React from 'react'
import { Link, graphql } from 'gatsby'
import Img from 'gatsby-image'

import Index from '../../components/index'

export default class ProjectIndexPage extends React.Component {
  state = { showModal: false, projectTitle: "" }

  toggleModal = (e) => {
    if (!this.state.showModal) {
      this.setState({
        projectTitle: e.title
      })
    }
    this.setState({
      showModal: !this.state.showModal
    })
  }

  render() {
    const { data } = this.props
    const { edges: posts } = data.allMarkdownRemark
    const covers = data.allFile.edges.reduce((arr, img) => {
      arr[img.node.name] = img.node.childImageSharp.sizes
      return arr
    }, {})
    const modalActive = this.state.showModal ? 'is-active' : ''

    return (
      <Index>
        <section className="projects section">
          <div className="container">
            <div className="columns is-multiline">
              {posts
                .map(({ node: post }) => (
                  <div
                    className="column is-half"
                    key={post.id}
                    data-modal="project"
                    onClick={() => this.toggleModal({title: post.frontmatter.title})}
                  >
                    <Img className="project" sizes={covers[post.frontmatter.cover]} />
                  </div>
                ))
              }
            </div>
          </div>
          <div className={`modal ${modalActive}`}>
            <div className="modal-background" onClick={this.toggleModal}></div>
            <div className="modal-content">
              <div className="has-text-centered is-size-2">
                {this.state.projectTitle}
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.toggleModal}
            ></button>
          </div>
        </section>
      </Index>
    )
  }
}

export const pageQuery = graphql`
  query ProjectIndexQuery {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: { frontmatter: { templateKey: { eq: "project" } } }
    ) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            title
            templateKey
            date(formatString: "MMMM DD, YYYY")
            cover
          }
        }
      }
    }
    allFile(filter: {
      sourceInstanceName: { eq: "images" }
      relativeDirectory: { eq: "projects" }
    }) {
      edges {
        node {
          name
          childImageSharp {
            sizes(maxWidth: 570) {
              ...GatsbyImageSharpSizes_withWebp_tracedSVG
            }
          }
        }
      }
    }
  }
`
