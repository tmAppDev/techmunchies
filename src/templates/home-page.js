import React from 'react'
import Content, { HTMLContent } from '../components/Content'
import { graphql } from 'gatsby'

import Home from '../components/home'

export const HomePageTemplate = ({ title, subtitle, content, contentComponent }) => {
  const PageContent = contentComponent || Content

  return (
    <Home>
      <div className="container has-text-centered">
        <h1 className="title">
          {title}
        </h1>
        <h2 className="subtitle is-size-7">
          {subtitle}
        </h2>
      </div>
    </Home>
  )
}

export default ({ data }) => {
  const { markdownRemark: post } = data

  return (
    <HomePageTemplate
      content={post.html}
      contentComponent={HTMLContent}
      title={post.frontmatter.title}
      subtitle={post.frontmatter.subtitle}
    />
  )
}

export const pageQuery = graphql`
  query HomePage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        title
        subtitle
      }
    }
  }
`
