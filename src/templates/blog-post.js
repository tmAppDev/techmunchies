import React from 'react'
import Content, { HTMLContent } from '../components/Content'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'

import Index from '../components/index'

export const BlogPostTemplate = ({
  content,
  contentComponent,
  description,
  title,
  date,
  tableOfContents,
}) => {
  const PostContent = contentComponent || Content

  return (
    <Index>
      <section className="post section">
        <Helmet
          script={[{
            src: 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'
          }]}
          script={[{
            innerHTML: '(adsbygoogle = window.adsbygoogle || []).push({google_ad_client: "ca-pub-8797724830363419",enable_page_level_ads: true});'
          }]}
        />
        <div className="container content">
          <div className="columns">
            <div className="column is-10 is-offset-1">
              <h1 className="title is-size-1 has-text-centered">
                {title}
              </h1>
              <h2 className="toc is-hidden-mobile is-hidden-tablet-only is-hidden-touch">Contents</h2>
              <div
                dangerouslySetInnerHTML={{ __html: tableOfContents }}
                className="toc is-hidden-mobile is-hidden-tablet-only is-hidden-touch"
              />
              <p className="is-size-3 has-text-justified">{description}</p>
              <hr />
              <p className="details has-text-centered">{date}</p>
              <PostContent className="post-content" content={content} />
            </div>
          </div>
        </div>
      </section>
    </Index>
  )
}

export default props => {
  const { markdownRemark: post } = props.data

  return (
    <BlogPostTemplate
      content={post.html}
      contentComponent={HTMLContent}
      description={post.frontmatter.description}
      helmet={<Helmet title={`Blog | ${post.frontmatter.title}`} />}
      title={post.frontmatter.title}
      date={post.frontmatter.date}
      tableOfContents={post.tableOfContents}
    />
  )
}

export const pageQuery = graphql`
  query BlogPostByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      tableOfContents
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        description
      }
    }
  }
`
