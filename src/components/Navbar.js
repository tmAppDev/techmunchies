import React from 'react'
import { Link } from 'gatsby'
import PropTypes from 'prop-types'

class Navbar extends React.Component {
  state = { showMenu: false }

  toggleMenu = () => {
    this.setState({
      showMenu: !this.state.showMenu
    })
  }

  render() {
    const menuActive = this.state.showMenu ? 'is-active' : '';
    const burgerActive = this.state.showMenu ? 'is-active' : '';
    
    return (
      <nav className="navbar is-transparent">
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            techmunchies
          </a>
          <a
            className={`navbar-burger ${burgerActive}`}
            role="button"
            aria-label="menu"
            aria-expanded="false"
            onClick={this.toggleMenu}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>

        <div id="site-nav" className={`navbar-menu ${menuActive}`}>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="field is-grouped">
                <Link
                  to="/"
                  className="navbar-item"
                  innerRef={(el) => { this.myLink = el }}
                >
                  home
                </Link>
                <Link
                  to="/projects/"
                  className="navbar-item"
                  activeClassName="item-active"
                  innerRef={(el) => { this.myLink = el }}
                >
                  projects
                </Link>
                <Link
                  to="/blog/"
                  className="navbar-item"
                  activeClassName="item-active"
                  innerRef={(el) => { this.myLink = el }}
                >
                  blog
                </Link>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default Navbar
