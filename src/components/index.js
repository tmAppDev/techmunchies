import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Navbar from './Navbar'
import Footer from './Footer'
import './all.scss'

const TemplateWrapper = ({ children }) => (
  <div className="app">
    <Helmet title="techmunchies" />
    <Navbar />
    <div>{children}</div>
    <Footer className="site-footer" />
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
