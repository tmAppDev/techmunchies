import React from 'react'
import Helmet from 'react-helmet'
import PropTypes from 'prop-types'

import Navbar from './Navbar'
import Footer from './Footer'
import './all.scss'

const TemplateWrapper = ({ children }) => (
  <div className="app">
    <section className="home hero is-fullheight">
      <Helmet title="techmunchies" />
      <div className="hero-head">
        <Navbar />
      </div>

      <div className="hero-body">
        {children}
      </div>

      <Footer className="hero-foot" />
    </section>
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
