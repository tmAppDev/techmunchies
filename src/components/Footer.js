import React from 'react'

const Footer = ({ className }) => (
  <div className={className + " has-text-centered is-size-7"}>
    &copy; techmunchies, inc
  </div>
)

export default Footer
