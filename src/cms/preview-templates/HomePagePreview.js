import React from 'react'
import { HomePageTemplate } from '../../templates/home-page'

const HomePagePreview = ({ entry, widgetFor }) => (
  <div className="app">
    <section className="home hero is-fullheight">
      <div className="hero-head">
      </div>

      <div className="hero-body">
        <HomePageTemplate
          content={widgetFor('body')}
          title={entry.getIn(['data', 'title'])}
          subtitle={entry.getIn(['data', 'subtitle'])}
        />
      </div>

      <div className="hero-foot"></div>
    </section>
  </div>
)

export default HomePagePreview
